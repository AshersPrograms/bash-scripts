﻿These are a collection of BASH(Bourne-Again Shell) scripts that I have collected or created over the years.

BASH scripts are a great way to automate repetitive tasks and streamline your day. The collection of BASH scripts that are gathered here over the years are a valuable resource for increasing my productivity and reduces errors.

These scripts can save you time and effort by providing a prewritten set of instructions that can be executed with a single command. They can be particularly useful in creating and performing complex tasks involving multiple commands, repetitive task, or require a specific set of variable sequencing of operations.

The example scripts are particularly useful files for those learning Linux command line programs. They provide practical examples, which could help users understand their functionality and potential in real life cases. By incorporating scripts into your workflow, you will quickly gain proficiency in your workflow.

Overall, the collections of BASH scripts that I have collected or created over the years can be a powerful asset to your toolkit learning how to create and use scripts. Will increase your productivity and reduce the amount of repetitive or complex task. You too can reduce error and increase efficiency, saving time and resources in the process.


example: Is a bash script that gets examples from cheat.sh and displays the results to the screen. This script should be thought of as and extension of whatis, man, and other Helping programs. Example will give you examples of how a program is used.


